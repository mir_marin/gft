class PresentationCard extends HTMLElement {
    constructor() {
        super()
        this.name = this.name
        this.email = this.email

        this.shadow = this.attachShadow({mode: 'open'})
        // this.checkDates(this._name, this._email, this.shadow)
    }

    checkDates(name, email, shadowParent){
        shadowParent.innerHTML = ""
        let divDatos = document.createElement('div')
        let divDato = document.createElement('div')

        if(name.toUpperCase() === 'MIRIAM'){
            divDatos.textContent = 'Soy yo'
        
        }else{
            divDatos.textContent = `Hola ${name}`;
        }
        if (email.search('@') !== -1) {
            if (email.includes('gmail.com')) {
                divDato.textContent = 'Perteneces a Skynet'
            } else {
                divDato.textContent = `Y tu correo es ${email}`
            }
        } else {
            alert('Revisa el formato del correo')
        }
        shadowParent.appendChild(divDatos)
        shadowParent.appendChild(divDato)
        
    }

    get name() {
        // console.log(this.getAttribute('name'))
        return this.getAttribute('name')
    }
    set name (value) {
        // console.log('(SET) name ' + value)
        this.setAttribute('name', value)
    }

    get email() {
        return this.getAttribute('email')
    }
    set email (value) {
        this._email = value
    } 

    attributeChangedCallback(name, oldValue, newValue) {
        // console.log('Cambian propiedades', name, oldValue, newValue)
        this.checkDates(this.name, this.email, this.shadow)
    }
 
    static get observedAttributes() {
        return ['name', 'email']
    }

 
 }
 
 customElements.define('presentacion-card-mnms', PresentationCard);
 
 