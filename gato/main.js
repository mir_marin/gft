class juegoGato extends HTMLElement {
    constructor() {
        super();
        this.div = document.createElement('div')
        this.init()
    }

    init(){ //aca esta lo que hace init
        const shadow = this.attachShadow({mode: 'open'})
        const div = document.createElement('div')
        shadow.appendChild(this.div)
        this.creaGato(3, this.div)
    }

    creaGato(botones = 4, parentNode){
        parentNode.innerHTML = ''
        let espacios = botones*botones
        const gato = [];
        let tiros = 0;
        for(let i=0; i < botones; i++) {
            gato[i] = new Array(botones)
            // llenando
            for(let j=0; j < botones; j++){
                gato[i][j] = '-'
            }
        }
        for(let i=0; i < espacios ; i++){
            let divBotones = document.createElement('button') 
            let p= document.createElement('p')
            let fila
            let columna
            divBotones.setAttribute('id', i+1)
            divBotones.addEventListener('click', ()=>{
                let valor_tiro = window.prompt('Tira')
                if(valor_tiro.toUpperCase() === 'X' || valor_tiro.toUpperCase() === 'O'){
                    divBotones.textContent = valor_tiro
                    let idCasilla = divBotones.getAttribute('id')
                    tiros = tiros + 1
                    switch(idCasilla){
                        case '1': {
                            fila = 0
                            columna = 0
                            gato[fila][columna] = valor_tiro.toUpperCase()
                            break
                        }
                        case espacios.toString() : {
                            fila = gato.length-1
                            columna = gato.length-1
                            gato[fila][columna] = valor_tiro.toUpperCase()
                            break
                        }
                        default:{
                            fila = parseInt(idCasilla/botones)
                            columna = idCasilla%botones
                            if(idCasilla < botones){
                                fila = 0
                                columna = idCasilla - 1 
                            }else{
                                if(columna === 0){
                                    fila = fila -1 
                                    columna = gato.length-1
                                }else{
                                    columna = columna - 1
                                }    
                            }
                            gato[fila][columna] = valor_tiro.toUpperCase()
                        }
                    }
                    if(tiros >= botones){
                        let este_tiro = gato[fila][columna]
                        let es_gato
                        es_gato = gato[fila].every( (valor) => {
                            return valor === este_tiro
                        })
                        
                        this.seHizoGato(es_gato, p, parentNode)

                        // vertical
                        for(i=0; i < botones ; i++){
                            if(este_tiro == gato[i][columna]){
                                es_gato = true
                            }else{
                                es_gato = false
                                break
                            }
                        }
                        this.seHizoGato(es_gato, p, parentNode)                      
  
                        // diagonal izquierda 
                        if( fila === columna){
                            for(let i = 0; i < botones; i++){
                                if(este_tiro == gato[i][i]){
                                    es_gato = true
                                }else{
                                    es_gato = false
                                    break
                                }                                
                            }
                            this.seHizoGato(es_gato, p, parentNode)     
                        }

                        // diagonal derecha 
                        if( fila+columna === gato.length-1){
                            for(let i = 0, j=gato.length-1; i < botones; i++, j--){
                                if(este_tiro == gato[i][j]){
                                    es_gato = true
                                }else{
                                    es_gato = false
                                    break
                                }                                
                            }
                            this.seHizoGato(es_gato, p, parentNode)
                        }

                    }
                }
                console.log(gato)
            })
            parentNode.appendChild(divBotones);
        }
        
    } 

    seHizoGato(valor, p, parentNode){
        if(valor){
            p.textContent = 'Termino el juego'
            parentNode.appendChild(p)
        }    
    }
 
    attributeChangedCallback(name, oldValue, newValue) {
        // console.log('Cambian las propiedades', name, oldValue, newValue);
        if(name=== 'side'){
            this.creaGato(parseInt(newValue), this.div)
        }
    }
    
    static get observedAttributes() {
        return ['side']
    }
 
 
 }
 
 
 customElements.define('gato-game-mnms', juegoGato);