class juegoGato extends HTMLElement {
    constructor() {
        super();
        this.div = document.createElement('div')
        this.init()
    }

    init(){ //aca esta lo que hace init
        const shadow = this.attachShadow({mode: 'open'})
        const div = document.createElement('div')
        shadow.appendChild(this.div)
        this.creaGato(3, this.div)
    }

    creaGato(botones = 4, parentNode){
        parentNode.innerHTML = ''
        let espacios = botones*botones
        const gato = [];
        let tiros = 0;
        let jugador_1 
        let jugador_2
        for(let i=0; i < botones; i++) {
            gato[i] = new Array(botones)
            // llenando
            for(let j=0; j < botones; j++){
                gato[i][j] = ''
            }
        }
        for(let i=0; i < espacios ; i++){
            let divBotones = document.createElement('button') 
            let fila
            let columna
            divBotones.setAttribute('id', i+1)
            divBotones.addEventListener('click', ()=>{
                let valor_tiro = window.prompt('Tira')
                if(tiros < 2){
                    if(valor_tiro !== null && valor_tiro !== ''){
                        if(tiros === 0){
                            tiros ++
                            jugador_1 = valor_tiro
                            this.storageValores(divBotones, tiros, gato, jugador_1, fila, columna, espacios, botones, parentNode)
                        }else if(tiros === 1 && valor_tiro !== jugador_1){
                            tiros ++
                            jugador_2 = valor_tiro
                            this.storageValores(divBotones, tiros, gato, jugador_2, fila, columna, espacios, botones, parentNode)
                        }else{
                            alert('Elige otro')
                        }
                    }
                }else{
                    if(valor_tiro !== null && valor_tiro !== ''){
                        if(tiros%2 === 0){
                            if(valor_tiro === jugador_1){
                                tiros ++
                                this.storageValores(divBotones, tiros, gato, jugador_1, fila, columna, espacios, botones, parentNode)
                            }else{
                                alert('Es el turno de: '+ jugador_1)
                            }                            
                        }else{
                            if(valor_tiro === jugador_2){
                                tiros ++
                                this.storageValores(divBotones, tiros, gato, jugador_2, fila, columna, espacios, botones, parentNode)
                            }else{
                                alert('Es el turno de: '+ jugador_2)
                            } 
                        }
                        // tiros ++
                    }
                }
                
                console.log(gato)
            })
            parentNode.appendChild(divBotones);
        }
        
    } 

    storageValores(divBotones, tiros, gato, valor_tiro, fila, columna, espacios, botones, parentNode){
        divBotones.textContent = valor_tiro
        let idCasilla = divBotones.getAttribute('id')
        //agregar en la matriz
        switch(idCasilla){
            case '1': {
                fila = 0
                columna = 0
                gato[fila][columna] = valor_tiro.toUpperCase()
                break
            }
            case espacios.toString() : {
                fila = gato.length-1
                columna = gato.length-1
                gato[fila][columna] = valor_tiro.toUpperCase()
                break
            }
            default:{
                fila = parseInt(idCasilla/botones)
                columna = idCasilla%botones
                if(idCasilla < botones){
                    fila = 0
                    columna = idCasilla - 1 
                }else{
                    if(columna === 0){
                        fila = fila -1 
                        columna = gato.length-1
                    }else{
                        columna = columna - 1
                    }    
                }
                gato[fila][columna] = valor_tiro.toUpperCase()
            }
        }
        this.logicaGato(tiros, botones, gato, fila, columna, parentNode)
    }    

    logicaGato(tiros, botones, gato, fila, columna, parentNode){
        if(tiros >= botones+botones -1 ){
            let este_tiro = gato[fila][columna]
            let es_gato
            es_gato = gato[fila].every( (valor) => {
                return valor === este_tiro
            })
            
            this.seHizoGato(es_gato, parentNode, este_tiro)

            // vertical
            for(let i=0; i < botones ; i++){
                if(este_tiro == gato[i][columna]){
                    es_gato = true
                }else{
                    es_gato = false
                    break
                }
            }
            this.seHizoGato(es_gato, parentNode, este_tiro)                      

            // diagonal izquierda 
            if( fila === columna){
                for(let i = 0; i < botones; i++){
                    if(este_tiro == gato[i][i]){
                        es_gato = true
                    }else{
                        es_gato = false
                        break
                    }                                
                }
                this.seHizoGato(es_gato, parentNode, este_tiro)     
            }

            // diagonal derecha 
            if( fila+columna === gato.length-1){
                for(let i = 0, j=gato.length-1; i < botones; i++, j--){
                    if(este_tiro == gato[i][j]){
                        es_gato = true
                    }else{
                        es_gato = false
                        break
                    }                                
                }
                this.seHizoGato(es_gato, parentNode, este_tiro)
            }
        }
    }    

    seHizoGato(valor, parentNode, este_tiro){
        if(valor){
            let p= document.createElement('p')
            p.textContent = 'Termino el juego. Gana ' + este_tiro
            parentNode.appendChild(p)
        }    
    }
 
    attributeChangedCallback(name, oldValue, newValue) {
        // console.log('Cambian las propiedades', name, oldValue, newValue);
        if(name=== 'side'){
            this.creaGato(parseInt(newValue), this.div)
        }
    }
    
    static get observedAttributes() {
        return ['side']
    }
 }
 
 
 customElements.define('gato-game-mnms', juegoGato);