class NuevoElementoWeb extends HTMLElement {
    constructor() {
        super();
        //los ciclos de vida se ejecutan primero 
        this.div = document.createElement('div');//declaracion de la variable llamada div 
        this.init(); // por buena practica se inicializa así (init puede ser cualquier otro nombre)
        // this.getPrompt();
        
 
    }

    getPrompt(){
        return window.prompt('Dame un valor')
    }

    init(){ //aca esta lo que hace init
        const shadow = this.attachShadow({mode: 'open'});
        const div = document.createElement('div');
        // div.textContent = 'Developer EnriqueLC';
        shadow.appendChild(this.div);
        this.createGame(this.getPrompt(), this.div); //para obtener el valor del prompt ... en lugar de un valor contante this.createGame(10, this.div);
    }

    //por si el usuario no ingresa algun dato || el parentNode es el padre donde se va a meter
    createGame(cells = 1, parentNode){
        let randomNumber = this.obtainRandomByOneNumber(cells);
        parentNode.innerHTML = ""; //limpiando al padre "la pantalla" en cada nuevo inicio
        // console.log('randomNumber', randomNumber)
        for(let i=0; i < cells ; i++){
            let divCasillas = document.createElement('div') // se crea un div 
            if(randomNumber === i){
                // se agrega el +1 para que el 'conteo' sea desde 1
                divCasillas.textContent = i+1;
                divCasillas.className = 'special' // se agrega la clase spacial al numero identico al random
                divCasillas.addEventListener('click', ()=>{
                    divCasillas.textContent = divCasillas.textContent + '  es el especial';
                    // reinicia el juego luego de 1 ms
                    setTimeout(() => {
                        this.createGame(this.getPrompt(), this.div); //vuelve a lanzar y a obtener el numero del promt
                    }, 1);
                });
            }else{
                divCasillas.textContent = i+1;
                // el click solo lo tendria el especual a menos que vaya el codigo de abajo
                divCasillas.addEventListener('click', ()=>{
                    divCasillas.textContent = 'Sigue intentando';
                });
            }
            //divCasillas.textContent = i; //ya que se maneja por referencia del anterior
            parentNode.appendChild(divCasillas);
            // se agrego un click al div Casillas y se lo manda al delegateClickHandler
            // divCasillas.addEventListener('click', this.delegateClickHandler); que es lo mismo que 
            // divCasillas.addEventListener('click', ()=>{
            //     console.log('click', i)
            // });


        }
    } 

    // delegateClickHandler(e){
    //     console.log(e);
    // }

    obtainRandomByOneNumber(number){
        return Math.floor(Math.random() * Math.floor(number));
    }

    connectedCallback() {
        console.log('Load NuevoElementoWeb agregado');
    }
 
    disconnectedCallback() {
        console.log('Removiendo NuevoElementoWeb');
    }
 
    attributeChangedCallback(name, oldValue, newValue) {
        // console.log('Cambian propiedades', name, oldValue, newValue);
        console.log('Cambian las propiedades', name, oldValue, newValue);
        if(name=== 'casillas'){
            this.createGame(newValue, this.div);
        }
        
    }
 
    static get observedAttributes() {
        return ['casillas'];
    }
 
 
 }
 
 customElements.define('nuevo-elemento-web', NuevoElementoWeb);
 
 